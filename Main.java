public class Main
{
    public static void main(String[] args)
    {
        int fib_num_1 = 1, fib_num_2 = 1, amount = 11, fib_sum = 0;
        for(int i = 0; i < amount; i++)
        {
            System.out.print(fib_num_1 + " ");
            fib_sum = fib_num_1 + fib_num_2;
            fib_num_1 = fib_num_2;
            fib_num_2 = fib_sum;
        }

        System.out.println();
        fib_num_1 = fib_num_2 = 1;
        int i = 0;
        while(i < amount)
        {
            System.out.print(fib_num_1 + " ");
            fib_sum = fib_num_1 + fib_num_2;
            fib_num_1 = fib_num_2;
            fib_num_2 = fib_sum;
            i++;
        }
    }
}